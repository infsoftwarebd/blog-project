@extends('front.includes.front_design')


@section('content')

    <section class="page-header">
        <div class="container-xl">
            <div class="text-center">
                <h1 class="mt-0 mb-2">{{ $category->category_name }}</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center mb-0">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $category->category_name }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>

    <!-- section main content -->
    <section class="main-content">
        <div class="container-xl">

            <div class="row gy-4">

                <div class="col-lg-8">

                    <div class="row gy-4">

                        @foreach($posts as $post)
                        <div class="col-sm-6">
                            <!-- post -->
                            <div class="post post-grid rounded bordered">
                                <div class="thumb top-rounded">
                                    <a href="{{ route('categorySingle', $post->category->slug) }}" class="category-badge position-absolute">{{ $post->category->category_name }}</a>
                                    <span class="post-format">
                                        <i class="icon-picture"></i>
                                    </span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            @if(!empty($post->image))
                                            <img src="{{ asset('public/uploads/posts/'.$post->image) }}" alt="{{ $post->post_title }}" />
                                            @else
                                                <img src="{{ asset('public/default/noimage.jpg') }}" alt="">
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <div class="details">
                                    <ul class="meta list-inline mb-0">
                                        <li class="list-inline-item"><a href="#">
                                                <img src="{{ asset('public/uploads/admin/'.$post->admin->image) }}" class="author" alt="author" width="30px" height="30px">
                                                  {{ $post->admin->name }}
                                            </a></li>
                                        <li class="list-inline-item">
                                            {{ toFormattedNepaliDate($post->created_at) }}
                                        </li>
                                    </ul>
                                    <h5 class="post-title mb-3 mt-3"><a href="blog-single.html">
                                            {{ $post->post_title }}
                                        </a></h5>
                                    <p class="excerpt mb-0">
                                        {{ Str::limit(strip_tags($post->post_content), 300, '.....') }}
                                    </p>
                                </div>

                            </div>
                        </div>
                        @endforeach


                    </div>

                    <nav>
                        <ul class="pagination justify-content-center">
                           {{ $posts->links() }}
                        </ul>
                    </nav>

                </div>
                <div class="col-lg-4">

                    <!-- sidebar -->
                    <div class="sidebar">
                        <!-- widget about -->

                        <!-- widget popular posts -->
                        <div class="widget rounded">
                            <div class="widget-header text-center">
                                <h3 class="widget-title">Popular Posts</h3>
                                <img src="{{ asset('public/frontend/images/wave.svg') }}" class="wave" alt="wave" />
                            </div>
                            <div class="widget-content">
                                <!-- post -->
                                @foreach($popular_posts as $post)
                                <div class="post post-list-sm circle">
                                    <div class="thumb circle">
                                        <span class="number">{{ $loop->index + 1 }}</span>
                                        <a href="blog-single.html">
                                            <div class="inner">
                                                @if(!empty($post->image))
                                                    <img src="{{ asset('public/uploads/posts/'.$post->image) }}" alt="{{ $post->post_title }}" />
                                                @else
                                                    <img src="{{ asset('public/default/noimage.jpg') }}" alt="">
                                                @endif
                                            </div>
                                        </a>
                                    </div>
                                    <div class="details clearfix">
                                        <h6 class="post-title my-0"><a href="blog-single.html">
                                                {{ $post->post_title }}
                                            </a></h6>
                                        <ul class="meta list-inline mt-1 mb-0">
                                            <li class="list-inline-item">
                                                {{ toFormattedNepaliDate($post->created_at) }}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>

                        <!-- widget categories -->
                        <div class="widget rounded">
                            <div class="widget-header text-center">
                                <h3 class="widget-title">Explore Topics</h3>
                                <img src="{{ asset('public/frontend/images/wave.svg') }}" class="wave" alt="wave" />

                            </div>
                            <div class="widget-content">
                                <ul class="list">
                                    @foreach($categories as $cat)
                                    <li><a href="{{ route('categorySingle', $cat->slug) }}">{{ $cat->category_name }}</a></li>
                                    @endforeach

                                </ul>
                            </div>

                        </div>

                        <!-- widget newsletter -->
                        <div class="widget rounded">
                            <div class="widget-header text-center">
                                <h3 class="widget-title">Newsletter</h3>
                                <img src="{{ asset('public/frontend/images/wave.svg') }}" class="wave" alt="wave" />

                            </div>
                            <div class="widget-content">
                                <span class="newsletter-headline text-center mb-3">Join 70,000 subscribers!</span>
                                <form>
                                    <div class="mb-2">
                                        <input class="form-control w-100 text-center" placeholder="Email address…" type="email">
                                    </div>
                                    <button class="btn btn-default btn-full" type="submit">Sign Up</button>
                                </form>
                                <span class="newsletter-privacy text-center mt-3">By signing up, you agree to our <a href="#">Privacy Policy</a></span>
                            </div>
                        </div>

                        <!-- widget post carousel -->



                        <!-- widget tags -->
                        <div class="widget rounded">
                            <div class="widget-header text-center">
                                <h3 class="widget-title">Tag Clouds</h3>
                                <img src="{{ asset('public/frontend/images/wave.svg') }}" class="wave" alt="wave" />

                            </div>
                            <div class="widget-content">
                                @foreach($tags as $tag)
                                <a href="#" class="tag">#{{ $tag->name }}</a>
                                @endforeach
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section>

@endsection
