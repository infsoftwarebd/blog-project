<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Index Page
Route::get('/', 'FrontEndController@index')->name('index');
// Category Post Page
Route::get('/category/{slug}', 'FrontEndController@categorySingle')->name('categorySingle');
// Post Single
Route::get('/post/{slug}', 'FrontEndController@postSingle')->name('postSingle');



Route::prefix('/admin')->group(function (){
    // Admin Login
    Route::match(['get', 'post'], '/login', 'AdminLoginController@login')->name('adminLogin');
    Route::group(['middleware' => 'admin'], function (){
        // Admin Dashboard
        Route::get('/dashboard', 'AdminLoginController@dashboard')->name('adminDashboard');
        // Admin Profile
        Route::get('/profile', 'AdminProfileController@profile')->name('profile');
        // Admin Profile Update
        Route::post('/profile/update/{id}', 'AdminProfileController@profileUpdate')->name('profileUpdate');
        // Admin Password
        Route::get('/profile/change_password', 'AdminProfileController@changePassword')->name('changePassword');
        // Check Current Password
        Route::post('/profile/check_password', 'AdminProfileController@chkUserPassword')->name('chkUserPassword');
        // Update Password
        Route::post('/profile/password_update/{id}', 'AdminProfileController@updatePassword')->name('updatePassword');

        // Category
        Route::get('/category', 'CategoryController@index')->name('category.index');
        Route::get('/category/add', 'CategoryController@add')->name('category.add');
        Route::post('/category/store', 'CategoryController@store')->name('category.store');
        Route::get('/category/table', 'CategoryController@dataTable')->name('table.category');
        Route::get('/category/show/{id}', 'CategoryController@show')->name('category.show');
        Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
        Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');
        Route::get('/delete-category/{id}', 'CategoryController@delete')->name('category.delete');
        Route::get('/export-category-excel', 'CategoryController@exportCategoryExcel')->name('exportCategoryExcel');

        // Tag
        Route::get('/tag', 'TagController@index')->name('tag.index');
        Route::get('/tag/add', 'TagController@add')->name('tag.add');
        Route::post('/tag/store', 'TagController@store')->name('tag.store');
        Route::get('/tag/table', 'TagController@dataTable')->name('table.tag');
        Route::get('/tag/edit/{id}', 'TagController@edit')->name('tag.edit');
        Route::post('/tag/update/{id}', 'TagController@update')->name('tag.update');
        Route::get('/delete-tag/{id}', 'TagController@delete')->name('tag.delete');

        // Category
        Route::get('/post', 'PostController@index')->name('post.index');
        Route::get('/post/add', 'PostController@add')->name('post.add');
        Route::post('/post/store', 'PostController@store')->name('post.store');
        Route::get('/post/table', 'PostController@dataTable')->name('table.post');
        Route::get('/post/show/{id}', 'PostController@show')->name('post.show');
        Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
        Route::post('/post/update/{id}', 'PostController@update')->name('post.update');
        Route::get('/delete-post/{id}', 'PostController@delete')->name('post.delete');

    });
    // Log Out
    Route::get('/logout', 'AdminLoginController@logout')->name('adminLogout');
});

Route::match(['get', 'post'], '/forget-password', 'AdminLoginController@forgetPassword')->name('forgetPassword');

Route::post('ckeditor', 'CkeditorFileUploadController@store')->name('ckeditor.store');
